<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.css">
    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>

<?php
 
//Ouverture du CSV avec fonction fopen(param r=lecture seule).
$fh = fopen("old.csv", "r");
 
//Création d'un tableau.
$csvData = array();
 
//Ajout des lignes CSV dans le Array.
while (($row = fgetcsv($fh, 0, ",")) !== FALSE) {
    $csvData[] = $row;

   json_encode($csvData);

}
?>


<h1>Computer Science figures</h1>
<div class="ui centered link cards">
  <div class="card">
    <div class="image">
      <img src=<?php echo $csvData[1][5] ?>>
    </div>
    <div class="content">
      <div class="header"><?php echo $csvData[1][0] ?> </div>
      <div class="meta">
        <a><?php echo $csvData[1][2] ?></a>
      </div>
      <div class="description">
      <?php echo $csvData[1][3] ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
      <?php echo $csvData[1][1] ?>
      </span>
    </div>
  </div>
  <div class="card">
    <div class="image">
      <img src=<?php echo $csvData[2][5] ?>>
    </div>
    <div class="content">
      <div class="header"><?php echo $csvData[2][0] ?></div>
      <div class="meta">
      <?php echo $csvData[2][2] ?>
      </div>
      <div class="description">
      <?php echo $csvData[2][3] ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
      <?php echo $csvData[2][1] ?>
      </span>
    </div>
  </div>
  <div class="card">
    <div class="image">
      <img src=<?php echo $csvData[3][5] ?>>
    </div>
    <div class="content">
      <div class="header"><?php echo $csvData[3][0] ?></div>
      <div class="meta">
      <?php echo $csvData[3][2] ?>
      </div>
      <div class="description">
      <?php echo $csvData[3][3] ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
      <?php echo $csvData[3][1] ?>
      </span>
    </div>
  </div>
</div>
<div class="ui centered link cards">
  <div class="card">
    <div class="image">
    <img src=<?php echo $csvData[4][5] ?>>
    </div>
    <div class="content">
      <div class="header"><?php echo $csvData[4][0] ?></div>
      <div class="meta">
      <?php echo $csvData[4][2] ?>
      </div>
      <div class="description">
      <?php echo $csvData[4][3] ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
      <?php echo $csvData[4][1] ?>
      </span>
    </div>
  </div>
  <div class="card">
  <div class="image">
    <img src=<?php echo $csvData[5][5] ?>>
    </div>
    <div class="content">
      <div class="header"><?php echo $csvData[5][0] ?></div>
      <div class="meta">
      <?php echo $csvData[5][2] ?>
      </div>
      <div class="description">
      <?php echo $csvData[5][3] ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
      <?php echo $csvData[5][1] ?>
      </span>
    </div>
  </div>
  <div class="card">
    <div class="image">
      <img src=<?php echo $csvData[6][5] ?>>
    </div>
    <div class="content">
    <?php echo $csvData[6][0] ?>
      <div class="meta">
      <?php echo $csvData[6][2] ?>
      </div>
      <div class="description">
      <?php echo $csvData[6][3] ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
      <?php echo $csvData[6][1] ?>
      </span>
    </div>
  </div>
</div>
<div class="ui centered card">
  <div class="image">
    <img src=<?php echo $csvData[7][5] ?>>
    </div>
    <div class="content">
      <div class="header"><?php echo $csvData[7][0] ?></div>
      <div class="meta">
      <?php echo $csvData[7][2] ?>
      </div>
      <div class="description">
      <?php echo $csvData[7][3] ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
      <?php echo $csvData[7][1] ?>
      </span>
    </div>
</div>
</body>
</html>